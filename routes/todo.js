const express = require('express');
const router = express.Router();
const TodoController = require('../controllers/todoController.js')

router.get('/', TodoController.listTodo)
router.get('/:id', TodoController.detailTodo)
router.post('/', TodoController.createTodo)
router.delete('/:id', TodoController.deleteTodo)

module.exports = router;