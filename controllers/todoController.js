const {Todo} = require('../models')

class todoController {

    static listTodo = async (req, res, next) => {
        try {
            const todo = await Todo.findAll();
            res.status(200).json(todo);
        }catch(err) {
            next(err)
        }

    }

    static detailTodo = async (req, res, next) => {
        try {
            const {id} = req.params;
            const todo = await Todo.findOne({
                where: {
                    id
                }
            });

            if(!todo) {
                throw {name: "ErrorNotFound"}
            }

            res.status(200).json(todo);
        } catch(err) {
            next(err);
        }
    }

    static createTodo = async (req, res, next) => {
        try {
            const {task, is_done} = req.body;

            await Todo.create({
                task,
                is_done
            })

            res.status(201).json({message: "Todo created"});
        } catch(err) {
            next(err);
        }

    }

    static deleteTodo = async (req, res, next) => {
        try {
            const {id} = req.params;

            const findTodo = await Todo.findOne({
                where: {
                    id
                }
            })

            if(!findTodo) {
                throw {name: "ErrorNotFound"}
            }

            await findTodo.destroy();

            res.status(200).json({message: "Todo deleted"})
        } catch(err) {  
            next(err);
        }
    }
}

module.exports = todoController