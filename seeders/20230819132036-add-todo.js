'use strict';

/** @type {import('sequelize-cli').Migration} */
module.exports = {
  async up (queryInterface, Sequelize) {
    await queryInterface.bulkInsert("Todos", 
      [
        {
          task: "Home Work Week 1",
          is_done: false,
          createdAt: new Date(),
          updatedAt: new Date()
        },
        {
          task: "Home Work Week 2",
          is_done: false,
          createdAt: new Date(),
          updatedAt: new Date()
        },
        {
          task: "Home Work Week 3",
          is_done: false,
          createdAt: new Date(),
          updatedAt: new Date()
        }
      ]
    , {})
    /**
     * Add seed commands here.
     *
     * Example:
     * await queryInterface.bulkInsert('People', [{
     *   name: 'John Doe',
     *   isBetaMember: false
     * }], {});
    */
  },

  async down (queryInterface, Sequelize) {
    await queryInterface.bulkDelete("Todos", null, {});
    /**
     * Add commands to revert seed here.
     *
     * Example:
     * await queryInterface.bulkDelete('People', null, {});
     */
  }
};
